module modernc.org/ccgo/v3

go 1.15

require (
	github.com/dustin/go-humanize v1.0.0
	golang.org/x/tools v0.0.0-20200827010519-17fd2f27a9e3
	modernc.org/cc/v3 v3.25.2
	modernc.org/mathutil v1.1.1
	modernc.org/opt v0.1.1
)
